package org.lenchan139.ddgsearcher.Class

import java.net.URLEncoder
class DuckDuckGoUtils{
    fun getInstantSuggestApiUrl(keyword: String): String{
        val query = URLEncoder.encode(keyword, "UTF-8")
        val url = String.format("https://ac.duckduckgo.com/ac/?q=%s&type=list", query)
        return url
    }
}