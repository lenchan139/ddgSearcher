package org.lenchan139.ddgsearcher

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.webkit.WebView
import org.lenchan139.ddgsearcher.Class.WebViewOverride
import android.view.KeyEvent.KEYCODE_ENTER
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.inputmethod.InputMethodManager
import android.webkit.WebChromeClient
import android.webkit.WebViewClient
import android.content.Intent
import android.net.Uri
import android.content.ActivityNotFoundException
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.PersistableBundle
import android.speech.RecognizerIntent
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startActivity
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.miguelcatalan.materialsearchview.MaterialSearchView
import org.jetbrains.anko.UI
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.find
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.lenchan139.ddgsearcher.Class.DuckDuckGoUtils
import org.lenchan139.ddgsearcher.Class.WebViewClientOverride
import java.net.URL
import java.nio.charset.Charset
import kotlin.system.exitProcess


class MainActivity : AppCompatActivity() {
    var webView: WebViewOverride? = null
    lateinit var searchView : MaterialSearchView
    val ddgUtils = DuckDuckGoUtils()
    lateinit var toolbar : Toolbar
    lateinit var toolbarContainer : FrameLayout
    lateinit var searchButton : EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        webView = findViewById(R.id.webView)
        toolbarContainer = findViewById(R.id.toolbar_container)
        searchButton = findViewById(R.id.searchButton)
        webView!!.settings!!.javaScriptEnabled = true
        webView!!.webViewClient = WebViewClientOverride()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            webView!!.settings!!.safeBrowsingEnabled = false
        }
        webView!!.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        val fab = findViewById<FloatingActionButton>(R.id.fab)
        initSearchView()
        fab.setOnClickListener { view ->
        }
        fab.visibility = View.GONE
        searchButton.setOnClickListener {
            searchView.showSearch()
        }

    }
    fun initSearchView(){
        searchView = findViewById<MaterialSearchView>(R.id.search_view)
        searchView.setVoiceSearch(true)
        searchView.setOnQueryTextListener(object : MaterialSearchView.OnQueryTextListener{
            override fun onQueryTextChange(newText: String?): Boolean {
                downloadInstantSuggest(newText)
                return false
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                searchDdg(query)
               return false
            }

        })
        searchView.setOnSearchViewListener(object : MaterialSearchView.SearchViewListener{

            override fun onSearchViewShown() {
                toolbarContainer.setBackgroundColor(Color.WHITE)
                webView?.visibility = View.GONE

            }

            override fun onSearchViewClosed() {
                toolbarContainer.setBackgroundColor(resources.getColor(R.color.colorPrimary))
                webView?.visibility = View.VISIBLE
            }

        })
    }

    override fun onPostCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onPostCreate(savedInstanceState, persistentState)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
         if (requestCode == MaterialSearchView.REQUEST_VOICE && resultCode == RESULT_OK) {
            val matches = data?.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches != null && matches.size > 0) {
                val searchWrd = matches.get(0);
                if (!TextUtils.isEmpty(searchWrd)) {
                    searchView.setQuery(searchWrd, false);
                }
            }

            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    override fun onPause() {
        super.onPause()
        webView?.onPause()
    }

    override fun onResume() {
        webView?.onResume()
        if(Intent.ACTION_ASSIST == intent.action) {
            searchView.requestFocus()
            searchView.performClick()
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        }

        super.onResume()
    }

    override fun onBackPressed() {
        if(searchView.isSearchOpen){
            searchView.closeSearch()
        }else {
            val intent = Intent(this, OpenFromAssistActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            finish()
        }
    }

    fun openExternal(u : String){

        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(u))
        try {
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
            Toast.makeText(this, "No Handler here.", Toast.LENGTH_SHORT).show()

        }
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)

        val item = menu.findItem(R.id.action_search)
        searchView.setMenuItem(item)
        searchView.showSearch(true)
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        return true
    }

    fun downloadInstantSuggest(keyword : String?){
        doAsync {
            if(keyword == null || keyword.isEmpty()){
                //if keyword empty, clear the list
                searchView.setSuggestions(null)
            }else{
                Log.v("DDG Suggest Url: ", ddgUtils.getInstantSuggestApiUrl(keyword))
                val content = URL(ddgUtils.getInstantSuggestApiUrl(keyword)).readText(Charset.defaultCharset())
                try{
                    val json = JSONArray(content)
                    if(json[0] == keyword){
                        val suggests = json[1] as JSONArray
                        val strSuggestsArray = Array<String>(suggests.length(), {""})
                        for(i in 0..suggests.length()-1){
                            strSuggestsArray[i] = suggests[i] as String
                        }
                        uiThread {
                            updateInstantSuggest(strSuggestsArray)
                        }

                    }else{
                        throw JSONException("Invalid JSON")
                    }

                }catch(e: JSONException){
                    e.printStackTrace()
                    searchView.setSuggestions(null)
                }
            }
        }
    }
    fun updateInstantSuggest(suggests: Array<String>){
        Log.v("suggests: " , suggests.joinToString("|"))
        searchView.setSuggestions(suggests)
    }
    fun searchDdg(keyword: String?){
        searchButton.setText(keyword)
        val url = "https://duckduckgo.com/?ko=-1&q=" + keyword
        Log.v("loadUrl: ", url)
        if(webView!!.loadUrlWithChecker(url)){
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
        }
        hideKeybord()
        webView!!.requestFocus()

}
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId


        if (id == R.id.action_settings) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    fun hideKeybord() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

}
